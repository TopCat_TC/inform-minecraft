# Inform mincraft script

Use python3

### Install

1. Create and use virtual environment (optional):
   `virtualenv venv; source venv/bin/activate`
   
2. Install requirements: 
   `pip install -r requirements.txt`
   
3. Run
   - `python inform-minecraft.py inform-config.yaml`
   Or with virutal environment
   - `venv/bin/python inform-minecraft.py inform-config.yaml`