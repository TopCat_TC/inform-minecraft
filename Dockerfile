############################################################
# Dockerfile to build inform-minecraft container images
# Based on python
############################################################
FROM python

# File Author / Maintainer
MAINTAINER TopCat_TC

RUN apt-get update
RUN pip install --upgrade pip
COPY scripts scripts/
RUN pip install -r scripts/requirements.txt

CMD scripts/inform-minecraft.py scripts/minecraft-user.yaml
